import pandas as pd
import matplotlib.pyplot as plt

from housing.data_loader import DataLoader

import warnings
warnings.filterwarnings('ignore')

# file = "/Users/jli/Documents/Projects/Kaggle/Cal_Housing/housing.csv"
data = DataLoader.load_housing_data()

# shape of data, (rows, columns)
print("====== Shape")
print(data.shape)
print("\n")

# get a quick description of the data, in particular the number of non-null, and each attribute’s type
print("====== Basic information")
print(data.info())
# The total_bedrooms variable have missing values
print("\n")

print("====== Missing values")
# check missing values
print(dict(data.isnull().sum()))
# print(data.isnull().sum())
print(data.isna().sum())
# isna() and isnull() does the same thing
print("\n")

# bool_series = pd.notnull(data['total_bedrooms'])
# print(data[bool_series])

# descriptive statistics
# print("====== Summary of numerical attributes")
# print(data.describe())
#
# subset of the columns
# print(data[['longitude', 'latitude', 'total_bedrooms', 'total_rooms']].describe(include='all'))
# print(data.describe(include='all'))

print("\n")

print("====== Distribution of categorical attributes")
print(data['ocean_proximity'].value_counts())

print("\n")

print("====== Plot distribution of the variables")
# Visualization of the variables' distribution
from housing.data_viewer import DataViewer
columns = ['longitude', 'housing_median_age', 'total_rooms',
           'total_bedrooms', 'households', 'median_income', 'longitude',
           'latitude', 'median_house_value']

DataViewer.distplot(data, 3, 3, columns)
'''
These histograms reveal a few things :
- The median house value was capped
- Machine Learning algorithms may learn that prices will never go beyond that limit "$500,000"
- Some variables are tail heavy : they extend much farther to the right of the median than to the left. 
  This may make it a bit harder for some Machine Learning algorithms to detect patterns.
'''

# zoom in on the target variable
plt.figure()
plt.hist(data['median_house_value'], bins=150)
plt.show()

# # The shape of noisy data
# print(data[data['median_house_value'] >= 500000].shape)
# print(data[data['median_house_value'] <= 500000].shape)
# # remove noisy values
# data = data[data['median_house_value'] <= 500000]

print( "====== Plot correlation matrix ")
corr_matrix = data.corr()
print(corr_matrix)
DataViewer.plot_corr_matrix(data)
# correlation between median house value and other variables
print(corr_matrix["median_house_value"].sort_values(ascending=False))

#-------------- visualization of geographic data
DataViewer.geo_plot(data)
# This geographical scatterplot of the data tells us that the housing prices are very much related to
# the location (close to the ocean) and to the population density