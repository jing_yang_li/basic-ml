import pandas as pd

class DataLoader:

    @classmethod
    def load_housing_data(cls):
        # df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/'
        #                  'housing/housing.data',
        #                  header=None,
        #                  sep='\s+')
        #
        # df.columns = ['CRIM', 'ZN', 'INDUS', 'CHAS',
        #               'NOX', 'RM', 'AGE', 'DIS', 'RAD',
        #               'TAX', 'PTRATIO', 'B', 'LSTAT', 'MEDV']
        #
        # return df
        file = "/Users/jli/Documents/Projects/Kaggle/Cal_Housing/housing.csv"
        data = pd.read_csv(file)
        return data

