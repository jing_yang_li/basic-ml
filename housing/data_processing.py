import pandas as pd
from pandas import DataFrame
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from housing.data_loader import DataLoader


class DataProcessing:

    @classmethod
    def fill_missing_values(self, data):
        # drop categorical column
        data_num = data.drop('ocean_proximity', axis=1)
        imputer = SimpleImputer(strategy="median")
        imputer.fit(data_num)
        data_transformed = imputer.transform(data_num)
        data_num_df = pd.DataFrame(data_transformed, columns=data_num.columns)
        return data_num_df

    @classmethod
    def add_new_features(cls, data):
        data["bedrooms_per_room"] = data["total_bedrooms"]/data["total_rooms"]
        data["rooms_per_household"] = data["total_rooms"]/data["households"]
        data["population_per_household"] = data["population"]/data["households"]

    @classmethod
    def handle_categorical_features(cls, data):
        encoder = LabelEncoder()
        housing_cat = data["ocean_proximity"]
        housing_cat_encoded = encoder.fit_transform(housing_cat)
        class_names = list(encoder.classes_)
        # one-hot encode
        encoder = OneHotEncoder()
        housing_cat_1hot = encoder.fit_transform(housing_cat_encoded.reshape(-1, 1))
        housing_cat_df = DataFrame(housing_cat_1hot.toarray(), columns=class_names)
        return housing_cat_df

    @classmethod
    def scale_data(cls, data):
        scaler = StandardScaler()
        scaler.fit(data)
        data_scaled = scaler.transform(data)
        data_scaled_df = DataFrame(data_scaled, columns=data.columns)
        return data_scaled_df

    @classmethod
    def remove_noisy_values(cls, data):
        data = data[data['median_house_value'] <= 500000]
        return data

    @classmethod
    def drop_missing_values(cls, data):
        data.dropna(axis=0, inplace=True)

    @classmethod
    def run(cls, data):
        data = cls.remove_noisy_values(data)

        # drop missing values
        data.dropna(axis=0, inplace=True)

        # drop categorical values
        data_num_df = data.drop('ocean_proximity', axis=1)

        # add new features
        cls.add_new_features(data_num_df)

        # scale data
        data_scaled_df = cls.scale_data(data_num_df)

        # handle categorical values
        housing_cat_df = cls.handle_categorical_features(data)

        # join
        data_num_df = pd.concat([data_scaled_df, housing_cat_df], axis=1)

        X = data_num_df[['median_income',
                         'housing_median_age',
                         'rooms_per_household',
                         'bedrooms_per_room', 'population_per_household',
                         'longitude', 'latitude',
                         '<1H OCEAN', 'INLAND', 'ISLAND', 'NEAR BAY', 'NEAR OCEAN'
                         ]]

        # x = data_num_df.values
        y = data['median_house_value'].values

        # split to train and test datasets
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

        return X_train, X_test, y_train, y_test


data = DataLoader.load_housing_data()
X_train, X_test, y_train, y_test = DataProcessing.run(data)
