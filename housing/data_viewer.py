import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

from housing.data_loader import DataLoader


class DataViewer:
    @classmethod
    def pair_plot(cls, df):
        sns.set(style='whitegrid', context='notebook')
        cols = ['LSTAT', 'INDUS', 'NOX', 'RM', 'MEDV']

        sns.pairplot(df[cols], size=2.5)
        plt.tight_layout()
        # plt.savefig('./figures/scatter.png', dpi=300)
        plt.show()

    @classmethod
    def heatmap(cls, df):
        cols = ['LSTAT', 'INDUS', 'NOX', 'RM', 'MEDV']
        cm = np.corrcoef(df[cols].values.T)
        sns.set(font_scale=1.5)
        hm = sns.heatmap(cm,
                         cbar=True,
                         annot=True,
                         square=True,
                         fmt='.2f',
                         annot_kws={'size': 15},
                         yticklabels=cols,
                         xticklabels=cols)

        # plt.tight_layout()
        # plt.savefig('./figures/corr_mat.png', dpi=300)
        plt.show()

    @classmethod
    def lin_regplot(cls, x, y, model):
        plt.scatter(x, y, c='lightblue')
        plt.plot(x, model.predict(x), color='red', linewidth=2)
        plt.show()

    @classmethod
    def distplot(cls, data, nrows, ncols, columns):
        '''
        Visualization of the variables' distribution
        '''
        rows = nrows
        cols = ncols

        fig, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(18, 12))

        columns = columns
        index = 0

        for i in range(rows):
            for j in range(cols):
                sns.distplot(data[columns[index]], ax=ax[i][j], bins=40)
                index += 1

        plt.show()

    @classmethod
    def plot_corr_matrix(cls, data):
        plt.figure(figsize=(15,7))
        corr_matrix = data.corr()
        sns.heatmap(corr_matrix, annot=True, cmap="YlGnBu")
        plt.show()

    @classmethod
    def geo_plot(cls, data):
        data.plot(kind='scatter',
                  x='longitude',
                  y='latitude',
                  alpha=0.4,
                  label='population',
                  figsize=(10, 7),
                  c='median_house_value',
                  s=data['population']/100,
                  cmap=plt.get_cmap('cubehelix'),
                  colorbar=True
                  )

        plt.legend()
        plt.show()

df = DataLoader.load_housing_data()
print(df.head())
# DataViewer.pair_plot(df)
# DataViewer.heatmap(df)
# TODO: other plots
# DataViewer.geo_plot(df)
