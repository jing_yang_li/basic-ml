from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
import numpy as np
from sklearn.model_selection import train_test_split

from housing.data_loader import DataLoader
from housing.data_viewer import DataViewer
from housing.training import Training

# load ans scale data
df = DataLoader.load_housing_data()
x = df[['RM', 'LSTAT']].values
y = df['MEDV'].values
# sc_x = StandardScaler()
# sc_y = StandardScaler()
# x_std = sc_x.fit_transform(x)
# y_std = sc_y.fit_transform(y[:, np.newaxis]).flatten()

# train
training = Training()
# lr_model = training.train_lr(x_std, y_std)
# DataViewer.lin_regplot(x_std, y_std, lr_model)

# predict
# training.predict_lr([5, 5], lr_model, sc_x, sc_y)

x = df.iloc[:, :-1].values # use all variables
y = df['MEDV'].values

# scale data
x_std, y_std, sc_x, sc_y = training.scale_data(x, y)

# split data
x_train, x_test, y_train, y_test = train_test_split(x_std, y_std, test_size=0.3, random_state=0)

# train - linear regression
# lr_model = training.train_lr(x_train, y_train)
# y_train_pred, y_train_pred_org  = training.predict_lr(x_train, lr_model, sc_x, sc_y)
# y_test_pred, y_test_pred_org = training.predict_lr(x_test, lr_model, sc_x, sc_y)

# train - polynomial regression
pr_model = training.train_pr(x_train, y_train)
y_train_pred, y_train_pred_org = training.predict_pr(x_train, pr_model, sc_x, sc_y)
y_test_pred, y_test_pred_org = training.predict_pr(x_test, pr_model, sc_x, sc_y)

print('MSE train: %.3f, test: %.3f' % (
    mean_squared_error(sc_y.inverse_transform(y_train), y_train_pred_org),
    mean_squared_error(sc_y.inverse_transform(y_test), y_test_pred_org)))
print('R^2 train: %.3f, test: %.3f' % (
    r2_score(y_train, y_train_pred),
    r2_score(y_test, y_test_pred)))

C_range = np.logspace(-2, 3, 10)
print(C_range)