import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from math import sqrt

from housing.data_processing import DataProcessing
from housing.data_loader import DataLoader
from housing.training import Training


# file = "/Users/jli/Documents/Projects/Kaggle/Cal_Housing/housing.csv"
#
# data = pd.read_csv(file)
#
# # ------------------ pre-processing
# # drop noisy values
# data = DataProcessing.remove_noisy_values(data)
#
# # drop missing values
# data.dropna(axis=0, inplace=True)
# print(data.isna().sum())
# # data_num_df = DataProcessing.fill_missing_values(data)
#
# # drop categorical values
# data_num_df = data.drop('ocean_proximity', axis=1)
#
# # add new features
# DataProcessing.add_new_features(data_num_df)
#
# # scale data
# data_scaled_df = DataProcessing.scale_data(data_num_df)
#
# # handle categorical values
# housing_cat_df = DataProcessing.handle_categorical_features(data)
#
# # data_num_df = data_scaled_df.join(housing_cat_df)
# data_num_df = pd.concat([data_scaled_df, housing_cat_df], axis=1)
#
# # X = df.iloc[:, :-1].values
# # X = df[['housing_median_age', 'total_rooms', 'total_bedrooms']]
#
# X = data_num_df[['median_income',
#                  'housing_median_age',
#                  'rooms_per_household',
#                  'bedrooms_per_room', 'population_per_household',
#                  'longitude', 'latitude',
#                  '<1H OCEAN', 'INLAND', 'ISLAND', 'NEAR BAY', 'NEAR OCEAN'
#                  ]]
#
# x = data_num_df.values
# y = data['median_house_value'].values
#
# # split to train and test datasets
# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

data = DataLoader.load_housing_data()
X_train, X_test, y_train, y_test = DataProcessing.run(data)

#==================== training ================================

trainer = Training()
final_model = trainer.train_rf_cv(X_train, y_train, features=list(X.columns))
# final_model = trainer.train_svm_cv(X_train, y_train)

# Test using test data
y_train_pred = final_model.predict(X_train)
y_test_pred = final_model.predict(X_test)

print('MSE train: %.3f, test: %.3f' % (
    sqrt(mean_squared_error(y_train, y_train_pred)),
    sqrt(mean_squared_error(y_test, y_test_pred))))
print('R^2 train: %.3f, test: %.3f' % (
    r2_score(y_train, y_train_pred),
    r2_score(y_test, y_test_pred)))

#TODO:
# - random forest [x]
# - svm [x]
#   - fix overfitting : tune the parameters through cross-validation [x]
#   - SVM (coarse and fine grid search) [x]
# - Regularized Linear Models []
# [x] drop missing values
# [] learning curves
# [] build pipeline


