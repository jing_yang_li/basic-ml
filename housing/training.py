from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.svm import SVR
import numpy as np


class Training:

    def train_lr(self, x, y):
        slr = LinearRegression()
        slr.fit(x, y)
        return slr

    def predict_lr(self, x, model, sc_x, sc_y):
        # x_std = sc_x.transform(np.array(x))
        y = model.predict(x)
        y_std = sc_y.inverse_transform(y)
        return y, y_std

    def scale_data(self, x, y):
        sc_x = StandardScaler()
        sc_y = StandardScaler()
        x_std = sc_x.fit_transform(x)
        y_std = sc_y.fit_transform(y[:, np.newaxis]).flatten()
        return x_std, y_std, sc_x, sc_y

    def train_pr(self, x, y, degree=2):
        '''
        Polynomial regression
        The features created include if degree=2:
        - The bias (the value of 1.0)
        - Values raised to a power for each degree (e.g. x^1, x^2, x^3, …)
        - Interactions between all pairs of features (e.g. x1 * x2, x1 * x3, …)
        '''
        quadratic = PolynomialFeatures(degree=degree)
        x_quad = quadratic.fit_transform(x)
        pr = LinearRegression()
        pr.fit(x_quad, y)
        return pr

    def predict_pr(self, x, model, sc_x, sc_y, degree=2):
        # x_std = sc_x.transform(np.array([x]))
        quadratic = PolynomialFeatures(degree=degree)
        x_quad = quadratic.fit_transform(x)

        y = model.predict(x_quad)
        y_std = sc_y.inverse_transform(y)
        return y, y_std

    def train_rf_cv(self, X_train, y_train, features):
        param_grid={
            'n_estimators': [3, 10, 30],
            'max_features': [2, 4, 6, 8, 10],
            # 'max_features': [2, 4],
            'bootstrap': [False, True],
            'max_depth': [5, 7, 9]
            # 'min_samples_leaf': [1, 2, 4],
            # 'min_samples_split': [2, 5, 10]
        }

        model = RandomForestRegressor()
        grid_search = GridSearchCV(model,
                                   param_grid,
                                   cv=5,
                                   scoring="neg_mean_squared_error"
                                   )
        grid_search.fit(X_train, y_train)

        final_model = grid_search.best_estimator_

        feature_importances = final_model.feature_importances_
        print(sorted(zip(feature_importances, features), reverse=True))

        return final_model

    def train_svm_cv(self, X_train, y_train):

        # coarse grid
        param_grid = {
            'C': np.arange(10, 1000, 50),
            # 'C': [10, 30, 100, 300, 1000],
            # 'gamma': [0.01, 0.03, 0.1, 0.3, 1, 3, 10],
            # 'epsilon': [0.01, 0.03, 0.1, 0.3, 1, 3, 10],
            # 'epsilon': np.arange(1, 6, 1),
            'kernel': ['rbf']
        }

        model = SVR()
        grid_search = GridSearchCV(model,
                                   param_grid,
                                   cv=5,
                                   # n_iter=5,
                                   scoring="neg_mean_squared_error"
                                   )

        grid_search.fit(X_train, y_train)

        # fine grid
        print(grid_search.best_params_)
        c = grid_search.best_params_['C']
        g = grid_search.best_params_['gamma']
        eps = grid_search.best_params_['epsilon']

        c_min = c * 0.8
        c_max = c * 1.2
        c_int = (c_max - c_min)/10

        g_min = g * 0.8
        g_max = g * 1.2
        g_int = (g_max - g_min)/10

        eps_min = eps * 0.8
        eps_max = eps * 1.2
        eps_int = (eps_max - eps_min)/10

        param_grid = {
            'C': np.arange(c_min, c_max, c_int),
            'gamma': np.arange(g_min, g_max, g_int),
            'epsilon': np.arange(eps_min, eps_max, eps_int),
            'kernel': ['rbf']
        }

        grid_search = GridSearchCV(
            model,
            param_grid,
            cv=5,
            scoring="neg_mean_squared_error"
        )

        grid_search.fit(X_train, y_train)
        print(grid_search.best_params_)

        return grid_search.best_estimator_
