
from iris.data_loader import DataLoader

class DataExplore:

    def __init__(self, dataset):
        self.dataset = dataset

    def summarize(self):
        print(self.dataset.shape)
        print(dataset.head(20))

        # descriptions
        # This includes the count, mean, the min and max values as well as some percentiles.
        print(dataset.describe())

        # class distribution
        # the number of instances (rows) that belong to each class
        print(dataset.groupby('class').size())

if __name__ == '__main__':
    dataset = DataLoader.load_iris()

    dataExplore = DataExplore(dataset)
    dataExplore.summarize() #150 instances and 5 attributes