from pandas import read_csv

class DataLoader:
    @staticmethod
    def load_iris():
        url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv"
        names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
        dataset = read_csv(url, names=names)
        return dataset

if __name__ == '__main__':
    dataset = DataLoader.load_iris()
    print(dataset.head())