
from matplotlib import pyplot
from pandas.plotting import scatter_matrix

from iris.data_loader import DataLoader

class DataPlots:
    def __init__(self, dataset):
        self.dataset = dataset

    def box_plot(self):

        # === box and whisker plots ===
        # plots of each individual variable
        # The box extends from the Q1 to Q3 quartile values of the data, with a line at the median (Q2).
        # The whiskers extend from the edges of box to show the range of the data. The position of the
        # whiskers is set by default to 1.5 * IQR (IQR = Q3 - Q1) from the edges of the box. Outlier
        # points are those past the end of the whiskers.

        # self.dataset.plot(kind='box', subplots=True, layout=(2,2), sharex=False, sharey=False)

        # or
        # self.dataset.boxplot() # all variables
        self.dataset.boxplot(column=['sepal-length', 'sepal-width']) # specified variables


        pyplot.show()


    def hist_plot(self):
        # histograms
        # create a histogram of each input variable to get an idea of the distribution
        self.dataset.hist()
        pyplot.show()

        # NOTE: It looks like perhaps two of the input variables have a Gaussian distribution.
        # This is useful to note as we can use algorithms that can exploit this assumption.

    def scatter_plot(self):
        # look at the interactions between the variables
        # scatter plot matrix
        scatter_matrix(dataset)
        pyplot.show()
        # NITE: The diagonal grouping of some pairs of attributes.
        # This suggests a high correlation and a predictable relationship.

if __name__ == '__main__':
    dataset = DataLoader.load_iris()

    dataPlot = DataPlots(dataset)

    # dataPlot.box_plot()
    # dataPlot.hist_plot()
    dataPlot.scatter_plot()