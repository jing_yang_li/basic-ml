
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from matplotlib import pyplot

from iris.data_loader import DataLoader


class Trainer:

    def __init__(self, dataset):
        self.dataset = dataset

    def train(self):
        # split-out validation dataset
        array = self.dataset.values
        X = array[:, 0:4]
        y = array[:, 4]

        # split the loaded dataset into two, 80% of which we will use to train, evaluate and select
        # among our models, and 20% that we will hold back as a validation dataset.
        X_train, X_validation, Y_train, Y_validation = train_test_split(X, y, test_size=0.2, random_state=1)

        # try 6 different classification algorithms
        models = []
        models.append(('LR', LogisticRegression(solver='liblinear', multi_class='ovr')))
        models.append(('LDA', LinearDiscriminantAnalysis()))
        models.append(('KNN', KNeighborsClassifier()))
        # models.append(('CART', DecisionTreeClassifier))
        models.append(('NB', GaussianNB()))
        models.append(('SVM', SVC(gamma='auto')))
        models.append(('RF', RandomForestClassifier(max_depth=5, random_state=0)))

        # evaluate each model in turn
        #   - use stratified 10-fold cross validation to estimate model accuracy
        #   - Stratified means that each fold or split of the dataset will aim to have the same distribution of
        #     example by class as exist in the whole training dataset.
        #   - set the random seed via the random_state argument to a fixed number to ensure that each algorithm is
        #     evaluated on the same splits of the training dataset.
        #   - We are using the metric of ‘accuracy‘ to evaluate models.
        #   - This is a ratio of the number of correctly predicted instances divided by the total number of instances
        #     in the dataset multiplied by 100 to give a percentage (e.g. 95% accurate).
        results = []
        names = []
        for name, model in models:
            kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
            cv_results = cross_val_score(model, X_train, Y_train, cv=kfold, scoring='accuracy')
            results.append(cv_results)
            names.append(name)
            print('%s %f (%f)' % (name, cv_results.mean(), cv_results.std()))


        # select the best model
        #   - In this case, we can see that it looks like Support Vector Machines (SVM) has the largest
        #     estimated accuracy score at about 0.98 or 98%.

        # Compare Algorithms
        # We can also create a plot of the model evaluation results and compare the spread and the mean
        # accuracy of each model. There is a population of accuracy measures for each algorithm because
        # each algorithm was evaluated 10 times (via 10 fold-cross validation).
        # A useful way to compare the samples of results for each algorithm is to create a box and whisker
        # plot for each distribution and compare the distributions.
        pyplot.boxplot(results, labels=names)
        pyplot.title('Algorithm Comparison')
        pyplot.show()
        # We can see that the box and whisker plots are squashed at the top of the range, with many evaluations
        # achieving 100% accuracy, and some pushing down into the high 80% accuracies.

        # Make predictins on validation dataset
        # We can fit the model on the entire training dataset and make predictions on the validation dataset.
        # Make predictions on validation dataset
        model = SVC(gamma='auto')
        model.fit(X_train, Y_train)
        #TODO: can save the model and load it later
        predictions = model.predict(X_validation)


        # Evaluate predictions
        print(accuracy_score(Y_validation, predictions))
        # the accuracy is 0.966 or about 96% on the hold out dataset.

        print(confusion_matrix(Y_validation, predictions))
        # The confusion matrix provides an indication of the three errors made.

        print(classification_report(Y_validation, predictions))
        # the classification report provides a breakdown of each class by precision, recall, f1-score and
        # support showing excellent results (granted the validation dataset was small).

if __name__ == '__main__':
    dataset = DataLoader.load_iris()

    trainer = Trainer(dataset)
    trainer.train()